# C2C-service

<h2>Pre-requisites before running the app</h2>

<li>Customer is already registered in the backend</li><br/>
<li>Categories are already present in backend. And products belong to two levels categories</li><br/>

<br/>

<h1>Functionalities Offered by the WebApp</h1>

<li>Login for customer</li><br/>
<li>Ability for customer to register the products for a category and its corresponding sub-category</li><br/>
<li>Ability for Customers to view current auctions based on categories and products</li><br/>
<li>Customer can wishlist a product to view its bids to decide his bid price </li><br/>
<li>Customer can bid a product and request with a message to seller-customer</li><br/>
<li>Seller can view different bids at different stages defined in dashboard and decide the appropriate bid based on defined attributes such as date, price, customer rating</li><br/>
<li>Customer can view different bids at different stages defined in dashboard</li><br/>
<li>Customer caan confirm the bid and pay for the same, currently with just pay. After successful transaction customer can view all the successful transactions and customer will be rated for genuiness filter(which will be later used in bid list for seller-customer)</li><br/>
