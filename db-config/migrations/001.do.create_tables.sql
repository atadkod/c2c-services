create extension if not exists "uuid-ossp";

CREATE TABLE users (
	uuid uuid NOT NULL DEFAULT uuid_generate_v1() PRIMARY KEY,
	firstname text NOT NULL,
	lastname text NOT NULL,
    email text NOT NULL Unique,
    hash_password text NOT NULL,
    address text NOT NULL,
    phoneno  bigint NOT NULL,
    watchlist jsonb,
    total_rating_count int,
    completed_orders_count int,
    created_at timestamptz NULL DEFAULT timezone('UTC'::text, CURRENT_TIMESTAMP),
	updated_at timestamptz NULL DEFAULT timezone('UTC'::text, CURRENT_TIMESTAMP)
);

CREATE TABLE products (
    uuid uuid NOT NULL DEFAULT uuid_generate_v1() PRIMARY KEY,
    name text NOT NULL,
    category_id uuid NOT NULL,
    description text,
    image text,
    price bigint,
    brand_name text,
    start_time timestamptz,
    end_time timestamptz,
    availability boolean,
    seller_id uuid NOT NULL,
    created_at timestamptz NULL DEFAULT timezone('UTC'::text, CURRENT_TIMESTAMP),
	updated_at timestamptz NULL DEFAULT timezone('UTC'::text, CURRENT_TIMESTAMP)
);

CREATE TABLE category (
    id uuid NOT NULL DEFAULT uuid_generate_v1() PRIMARY KEY,
    name text NOT NULL Unique,
    parent_id uuid DEFAULT null,
    created_at timestamptz NULL DEFAULT timezone('UTC'::text, CURRENT_TIMESTAMP),
	updated_at timestamptz NULL DEFAULT timezone('UTC'::text, CURRENT_TIMESTAMP),
    is_active boolean DEFAULT TRUE
);

CREATE TABLE orders (
    uuid uuid NOT NULL DEFAULT uuid_generate_v1() PRIMARY KEY,
    product_id uuid NOT NULL,
    buyer_id uuid NOT NULL,
    seller_id uuid NOT NULL,
    price bigint,
    created_at timestamptz NULL DEFAULT timezone('UTC'::text, CURRENT_TIMESTAMP),
	updated_at timestamptz NULL DEFAULT timezone('UTC'::text, CURRENT_TIMESTAMP)
);
