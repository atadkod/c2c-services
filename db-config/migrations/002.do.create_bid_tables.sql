create extension if not exists "uuid-ossp";
 
CREATE TABLE bid_entry (
    bid_id uuid NOT NULL DEFAULT uuid_generate_v1() PRIMARY KEY,
    product_id uuid NOT NULL,
    buyer_id uuid NOT NULL,
    seller_id uuid NOT NULL,
    base_price bigint,
    bid_price bigint,
    message text,
    is_active boolean,
    created_at timestamptz NULL DEFAULT timezone('UTC'::text, CURRENT_TIMESTAMP),
	updated_at timestamptz NULL DEFAULT timezone('UTC'::text, CURRENT_TIMESTAMP)
);

CREATE TABLE allocated_bids (
    alloc_id uuid NOT NULL DEFAULT uuid_generate_v1() PRIMARY KEY,
    bid_id uuid NOT NULL,
    product_id uuid NOT NULL,
    buyer_id uuid NOT NULL,
    seller_id uuid NOT NULL,
    price bigint,
    buyer_status boolean,
    created_at timestamptz NULL DEFAULT timezone('UTC'::text, CURRENT_TIMESTAMP),
	updated_at timestamptz NULL DEFAULT timezone('UTC'::text, CURRENT_TIMESTAMP)
);
