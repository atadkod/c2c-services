create extension if not exists "uuid-ossp";

CREATE TABLE watchlist (
    id uuid NOT NULL DEFAULT uuid_generate_v1() PRIMARY KEY,
    product_id uuid NOT NULL,
    buyer_id uuid NOT NULL,
    threshold numeric,
    threshold_status boolean,
    created_at timestamptz NULL DEFAULT timezone('UTC'::text, CURRENT_TIMESTAMP),
	updated_at timestamptz NULL DEFAULT timezone('UTC'::text, CURRENT_TIMESTAMP)
);
