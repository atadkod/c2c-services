const postgrator = require("postgrator");
const path = require("path");

const getPostgrator = require("../getPostgrator");

jest.mock("path");
jest.mock("postgrator");

describe("Generate Postgrator", () => {
  const config = {
    DB_HOST: "//test",
    DB_PORT: "5432",
    DB_NAME: "testDb",
    DB_USER: "testUser",
    DB_PASSWORD: "testPwd",
    DB_SCHEMA: "catalog",
    INTEGRATION_TEST: false
  };
  const action = "do";

  it("should instantiate postgrator with proper migrations file on db migrations", () => {
    const migrationDirectory = "../migrations";
    path.join = jest.fn(() => migrationDirectory);

    getPostgrator(config, action);

    const {
      DB_HOST,
      DB_PORT,
      DB_NAME,
      DB_SCHEMA,
      DB_USER,
      DB_PASSWORD
    } = config;

    const expectedConfig = {
      migrationDirectory,
      schemaTable: "schemaversion",
      driver: "pg",
      host: DB_HOST,
      port: DB_PORT,
      database: DB_NAME,
      username: DB_USER,
      password: DB_PASSWORD,
      currentSchema: DB_SCHEMA,
      action
    };
    expect(path.join).toHaveBeenCalledWith(
      expect.any(String),
      migrationDirectory
    );
    expect(postgrator).toHaveBeenCalledWith(expectedConfig);
  });

  it("should instantiate postgrator with proper migration files on integration tests", () => {
    const migrationDirectory = "../migrations";
    path.join = jest.fn(() => migrationDirectory);
    config.INTEGRATION_TEST = true;

    getPostgrator(config, action);

    const {
      DB_HOST,
      DB_PORT,
      DB_NAME,
      DB_SCHEMA,
      DB_USER,
      DB_PASSWORD
    } = config;

    const expectedConfig = {
      migrationDirectory,
      schemaTable: "schemaversion",
      driver: "pg",
      host: DB_HOST,
      port: DB_PORT,
      database: DB_NAME,
      username: DB_USER,
      password: DB_PASSWORD,
      currentSchema: DB_SCHEMA,
      action
    };
    expect(path.join).toHaveBeenCalledWith(
      expect.any(String),
      migrationDirectory
    );
    expect(postgrator).toHaveBeenCalledWith(expectedConfig);
  });

  it("When INTEGRATION_TEST env var is true, migration should be from integration folder", () => {
    // We are mocking the migration path call to use wrong int folder
    const migrationDirectory = "../w-integration-migrations";
    path.join = jest.fn(() => migrationDirectory);
    config.INTEGRATION_TEST = true;

    getPostgrator(config, action);

    const {
      DB_HOST,
      DB_PORT,
      DB_NAME,
      DB_SCHEMA,
      DB_USER,
      DB_PASSWORD
    } = config;

    const expectedConfig = {
      migrationDirectory,
      schemaTable: "schemaversion",
      driver: "pg",
      host: DB_HOST,
      port: DB_PORT,
      database: DB_NAME,
      username: DB_USER,
      password: DB_PASSWORD,
      currentSchema: DB_SCHEMA,
      action
    };

    // Should not use the mocked wrong path for migration as env var IT is true
    expect(path.join).not.toHaveBeenCalledWith(
      expect.any(String),
      migrationDirectory
    );
    expect(postgrator).toHaveBeenCalledWith(expectedConfig);
  });
});
