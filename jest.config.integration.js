// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

module.exports = {
  testEnvironment: "node",
  clearMocks: true,
  rootDir: "./test/integration",
  testMatch: ["<rootDir>/**/?(*.)test.js"],
  globalSetup: "<rootDir>/setup.js",
  globalTeardown: "<rootDir>/teardown.js"
};
