const { OK } = require("http-status-codes");
// const { postAllocatedBidSchema } =require("./schema/bidSchema");
const allocatedBididRepository = require("../plugins/repository/v1/allocatedBidRepository");
const authenticateFactory = require("../utilities/authenticate");

const allocatedBidAPI = async fastify => {
  fastify.get(
    "/sellerandproduct",
    { preHandler: authenticateFactory() },
    async (req, reply) => {
      const sellerId = req.user.userId; //resolve from firbase auth
      fastify.log.info({
        message:
          "Request to fetch all bids for sellerId:" +
          sellerId +
          " for productId: " +
          req.query.id
      });

      const res = await allocatedBididRepository.getBySellerAndProductId(
        fastify,
        sellerId,
        req.query.id
      );

      reply.code(OK).send(res);
    }
  );

  fastify.get(
    "/seller",
    { preHandler: authenticateFactory() },
    async (req, reply) => {
      const sellerId = req.user.userId; //resolve from firbase auth
      fastify.log.info({
        message: "Request to fetch all bids for sellerId:" + sellerId
      });

      const res = await allocatedBididRepository.getBySellerId(
        fastify,
        sellerId
      );

      reply.code(OK).send(res);
    }
  );

  fastify.get(
    "/buyer",
    { preHandler: authenticateFactory() },
    async (req, reply) => {
      const buyerId = req.user.userId; //resolve from firbase auth
      fastify.log.info({
        message: "Request to fetch all bids for buyerId:" + buyerId
      });

      const res = await allocatedBididRepository.getByBuyerId(fastify, buyerId);

      reply.code(OK).send(res);
    }
  );

  fastify.get("/product", async (req, reply) => {
    fastify.log.info({
      message: "Request to fetch all bids for productId:" + req.query.id
    });

    const res = await allocatedBididRepository.getByProductId(
      fastify,
      req.query.id
    );

    reply.code(OK).send(res);
  });

  //   fastify.post(
  //     "/",
  //     { schema: postAllocatedBidSchema, preHandler: authenticateFactory() },
  //     async (req, reply) => {
  //       console.log(req.body);
  //       const buyerId = req.user.userId; //resolve from firbase auth
  //       fastify.log.info({
  //         message:
  //           "Request to add bid entry" +
  //           JSON.stringify(req.body) +
  //           "by buyer:" +
  //           buyerId
  //       });

  //       const res = await allocatedBididRepository.insert(fastify, { ...req.body, buyerId });

  //       reply.code(OK).send(res);
  //     }
  //   );
};

module.exports = allocatedBidAPI;
module.exports.autoPrefix = "/allocated";
