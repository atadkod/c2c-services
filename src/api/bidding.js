const { OK } = require("http-status-codes");
const {
  postBiddingSchema,
  postAllocatedBidSchema
} = require("./schema/bidSchema");

const bidRepository = require("../plugins/repository/v1/biddingResitory");
const bidService = require("../plugins/services/biddingService");
const authenticateFactory = require("../utilities/authenticate");

const biddingAPI = async fastify => {
  fastify.get(
    "/seller",
    { preHandler: authenticateFactory() },
    async (req, reply) => {
      const sellerId = req.user.userId; //resolve from firbase auth
      fastify.log.info({
        message:
          "Request to fetch all bids for sellerId:" +
          sellerId +
          " for productId: " +
          req.query.id
      });

      const res = await bidRepository.getBySellerAndProductId(
        fastify,
        sellerId,
        req.query.id
      );

      reply.code(OK).send(res);
    }
  );

  fastify.get(
    "/buyer",
    { preHandler: authenticateFactory() },
    async (req, reply) => {
      const buyerId = req.user.userId; //resolve from firbase auth
      fastify.log.info({
        message: "Request to fetch all bids for buyerId:" + buyerId
      });

      const res = await bidRepository.getByBuyerId(fastify, buyerId);

      reply.code(OK).send(res);
    }
  );

  fastify.get("/product", async (req, reply) => {
    fastify.log.info({
      message: "Request to fetch all bids for productId:" + req.query.id
    });

    const res = await bidRepository.getByProductId(fastify, req.query.id);

    reply.code(OK).send(res);
  });

  fastify.get("/product/highest", async (req, reply) => {
    fastify.log.info({
      message: "Request to fetch highest bid for productId:" + req.query.id
    });

    const res = await bidRepository.getHighestBidByProductId(
      fastify,
      req.query.id
    );

    reply.code(OK).send(res);
  });

  fastify.post(
    "/",
    { schema: postBiddingSchema, preHandler: authenticateFactory() },
    async (req, reply) => {
      console.log(req.body);
      const buyerId = req.user.userId; //resolve from firbase auth
      fastify.log.info({
        message:
          "Request to add bid entry" +
          JSON.stringify(req.body) +
          "by buyer:" +
          buyerId
      });

      const res = await bidService.insert(fastify, { ...req.body, buyerId });

      reply.code(OK).send(res);
    }
  );

  /*
   *** Workflows for confirmation of bids from seller and buyer
   */
  fastify.post(
    "/buyconfirmed",
    { schema: postAllocatedBidSchema, preHandler: authenticateFactory() },
    async (req, reply) => {
      const buyerId = req.user.userId; //resolve from firbase auth
      const confirmed = true;
      fastify.log.info({
        message:
          "Confirmation from buyer for the allocated_bid" +
          JSON.stringify(req.body) +
          "by buyer:" +
          buyerId
      });
      const res = await bidService.ActionByBuyer(fastify, {
        ...req.body,
        buyerId,
        confirmed
      });

      reply.code(OK).send(res);
    }
  );

  fastify.post(
    "/buydenied",
    { schema: postAllocatedBidSchema, preHandler: authenticateFactory() },
    async (req, reply) => {
      const buyerId = req.user.userId; //resolve from firbase auth
      const confirmed = false;
      fastify.log.info({
        message:
          "Denial from buyer for the allocated_bid" +
          JSON.stringify(req.body) +
          "by buyer:" +
          buyerId
      });
      const res = await bidService.ActionByBuyer(fastify, {
        ...req.body,
        buyerId,
        confirmed
      });

      reply.code(OK).send(res);
    }
  );

  fastify.post(
    "/sellconfirmed",
    { schema: postAllocatedBidSchema, preHandler: authenticateFactory() },
    async (req, reply) => {
      console.log(req.body);
      const sellerId = req.user.userId; //resolve from firbase auth
      fastify.log.info({
        message:
          "Confirmation from buyer for the bid" +
          JSON.stringify(req.body) +
          "by seller:" +
          sellerId
      });

      const res = await bidService.confirmedBySeller(fastify, {
        ...req.body,
        sellerId
      });

      reply.code(OK).send(res);
    }
  );
};

module.exports = biddingAPI;
module.exports.autoPrefix = "/bidding";
