const { OK } = require("http-status-codes");
const {
  getCategorySchema,
  postCategorySchema
} = require("./schema/categorySchema");

const categoryReportRepository = require("../plugins/repository/v1/categoryRepository");

const categoryAPI = async fastify => {
  fastify.get("/", async (req, reply) => {
    fastify.log.info({
      message: "Request to fetch all parent categories"
    });

    const res = await categoryReportRepository.getParentCategories(fastify);

    reply.code(OK).send(res);
  });

  fastify.get("/subCatergories", async (req, reply) => {
    fastify.log.info({
      message:
        "Request to fetch all sub categories for parent id:" + req.query.id
    });

    const res = await categoryReportRepository.getCategoryByParentId(
      fastify,
      req.query.id
    );

    reply.code(OK).send(res);
  });

  fastify.get("/getById", { schema: getCategorySchema }, async (req, reply) => {
    fastify.log.info({
      message: "Request to fetch catagory for parent id :" + req.query.id
    });

    const res = await categoryReportRepository.getCategoryById(
      fastify,
      req.query.id
    );

    reply.code(OK).send(res);
  });

  fastify.post("/", { schema: postCategorySchema }, async (req, reply) => {
    fastify.log.info({
      message: "Request to add catagory :" + req.body
    });
    console.log(req.body);
    const res = await categoryReportRepository.postCategory(fastify, req.body);

    reply.code(OK).send(res);
  });

  fastify.get("/name", { schema: getCategorySchema }, async (req, reply) => {
    fastify.log.info({
      message: "Request to fetch catagory for parent name :" + req.query.id
    });
    const res = await categoryReportRepository.getCategoryByName(
      fastify,
      req.query.id
    );
    reply.code(OK).send(res);
  });
};

module.exports = categoryAPI;
module.exports.autoPrefix = "/category";
