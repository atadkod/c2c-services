const { OK } = require("http-status-codes");
const {
  getParentCategories
} = require("../plugins/repository/v1/categoryRepository");

const demoAPI = async fastify => {
  fastify.get("/", async (req, reply) => {
    await getParentCategories(fastify);
    reply.code(OK).send({ hello: "world" });
  });
};

module.exports = demoAPI;
