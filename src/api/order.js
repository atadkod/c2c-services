const orderSchema = require("./schema/orderSchema");
const orderRepository = require("../plugins/repository/v1/orderRepository");
const { OK } = require("http-status-codes");
const authenticateFactory = require("../utilities/authenticate");

const orderAPI = async fastify => {
  fastify.addHook("preHandler", authenticateFactory());
  fastify.post(
    "/",
    { schema: orderSchema.postOrderSchema },
    async (req, reply) => {
      const id = req.user.userId;
      fastify.log.info({
        message: "Request to add order :" + req.body
      });
      const res = await orderRepository.postOrder(fastify, req.body, id);

      reply.code(OK).send(res);
    }
  );
  fastify.get("/sold", async (req, reply) => {
    const id = req.user.userId;
    fastify.log.info({
      message: "Request to get sold orders for user :" + id
    });
    const res = await orderRepository.getSoldOrders(fastify, id);

    reply.code(OK).send(res);
  });

  fastify.get("/bought", async (req, reply) => {
    const id = req.user.userId;
    fastify.log.info({
      message: "Request to get bought orders for user :" + id
    });
    const res = await orderRepository.getBoughtOrders(fastify, id);

    reply.code(OK).send(res);
  });

  fastify.get("/", async (req, reply) => {
    const id = req.user.userId;
    fastify.log.info({
      message: "Request to get all orders for user :" + id
    });
    const res = await orderRepository.getOrders(fastify, id);

    reply.code(OK).send(res);
  });
};

module.exports = orderAPI;
module.exports.autoPrefix = "/order";
