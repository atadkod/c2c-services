const { OK } = require("http-status-codes");
const { postProductBody, putProductBody } = require("./schema/productSchema");

const productRepository = require("../plugins/repository/v1/productRepository");
const authenticateFactory = require("../utilities/authenticate");
const { UNAUTHORIZED } = require("http-status-codes");
const jwt = require("jsonwebtoken");

const productAPI = async fastify => {
  fastify.get("/all", async (req, reply) => {
    fastify.log.info({
      message: "Request to fetch all products"
    });

    const authInfo = req.headers["x-access-token"];
    if (authInfo) {
      try {
        const decoded = jwt.verify(authInfo, process.env.TOKEN_KEY);
        req.user = decoded;
      } catch (err) {
        const error = { status: UNAUTHORIZED, detail: "ACCESS DENIED" };
        console.log(error);
      }
    }
    const sellerId = req.user ? req.user.userId : null;
    const res1 = await productRepository.getAllBuyerProduct(fastify, sellerId);
    const res = refactorProducts(res1);
    reply.code(OK).send(res);
  });

  fastify.get(
    "/seller",
    { preHandler: authenticateFactory() },
    async (req, reply) => {
      fastify.log.info({
        message:
          "Request to fetch all products for sellerId: " + req.user.userId
      });
      const sellerId = req.user.userId;
      const res1 = await productRepository.getAllSellerProduct(
        fastify,
        sellerId
      );
      reply.code(OK).send(res1);
    }
  );
  fastify.get("/catid/:id", async (req, reply) => {
    fastify.log.info({
      message: "Request to fetch all products by category id:" + req.query.id
    });

    const res1 = await productRepository.getByCategoryId(fastify, req.query.id);
    const res = refactorProducts(res1);
    reply.code(OK).send(res);
  });

  fastify.get("/", async (req, reply) => {
    fastify.log.info({
      message: "Request to fetch products by productId:" + req.query.id
    });

    const res = await productRepository.getByProductId(fastify, req.query.id);
    reply.code(OK).send(res);
  });

  fastify.get("/allocated", async (req, reply) => {
    fastify.log.info({
      message: "Request to fetch products by productId:" + req.query.id
    });

    const res = await productRepository.getByProductIdWithoutAvailability(
      fastify,
      req.query.id
    );
    reply.code(OK).send(res);
  });

  fastify.post(
    "/",
    { schema: postProductBody, preHandler: authenticateFactory() },
    async (req, reply) => {
      const sellerId = req.user.userId; //resolve from firbase auth

      fastify.log.info({
        message:
          "Request to add product for product:" + { ...req.body, sellerId }
      });
      console.log(req.body);
      const res = await productRepository.insert(fastify, {
        ...req.body,
        sellerId
      });

      reply.code(OK).send(res);
    }
  );

  fastify.put(
    "/",
    { schema: putProductBody, preHandler: authenticateFactory() },
    async (req, reply) => {
      const sellerId = req.user.userId; //resolve from firbase auth
      fastify.log.info({
        message:
          "Request to add product for product:" + { ...req.body, sellerId }
      });

      const res = await productRepository.update(fastify, {
        ...req.body,
        sellerId
      });

      reply.code(OK).send(res);
    }
  );
};

const refactorProducts = res => {
  const products = [];
  let inner = [];
  if (res != null) {
    res.forEach(element => {
      if (inner.length == 3) {
        products.push(inner);
        inner = [];
      }
      inner.push(element);
    });
    products.push(inner);
  }
  return products;
};

module.exports = productAPI;
module.exports.autoPrefix = "/product";
