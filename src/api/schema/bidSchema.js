const postBiddingBodySchema = {
  type: "object",
  additionalProperties: false,
  properties: {
    productId: { type: "string", minLength: 1 },
    sellerId: { type: "string", minLength: 1 },
    basePrice: { type: "integer", minLength: 1 },
    bidPrice: { type: "integer", minLength: 1 },
    message: { type: "string" },
    isActive: { type: "boolean" }
  }
};

const postAllocatedBidBodySchema = {
  type: "object",
  additionalProperties: false,
  properties: {
    bidId: { type: "string", minLength: 1 },
    productId: { type: "string", minLength: 1 },
    buyerId: { type: "string", minLength: 1 },
    sellerId: { type: "string", minLength: 1 },
    price: { type: "integer", minLength: 1 },
    buyerStatus: { type: "boolean" }
  }
};

module.exports = {
  postBiddingSchema: {
    body: postBiddingBodySchema
  },
  postAllocatedBidSchema: {
    body: postAllocatedBidBodySchema
  }
};
