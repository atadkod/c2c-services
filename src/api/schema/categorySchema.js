const getCategoryQuerySchema = {
  type: "object",
  additionalProperties: false,
  properties: {
    id: { type: "string", minLength: 1 }
  }
};

const postCategoryBodySchema = {
  type: "object",
  additionalProperties: false,
  properties: {
    name: { type: "string", minLength: 1 },
    parentId: { type: ["string", "null"] },
    isActive: { type: ["boolean", "null"] }
  }
};

module.exports = {
  getCategorySchema: {
    query: getCategoryQuerySchema
  },
  postCategorySchema: {
    body: postCategoryBodySchema
  }
};
