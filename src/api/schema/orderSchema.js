const getOrdersByUserSchema = {
  type: "object",
  additionalProperties: false,
  properties: {
    id: { type: "string", minLength: 1 }
  }
};

const postOrderBodySchema = {
  type: "object",
  additionalProperties: false,
  properties: {
    productId: { type: "string", minLength: 1 },
    buyerId: { type: "string", minLength: 1 },
    price: { type: "integer", minLength: 1 }
  }
};

module.exports = {
  getOrderSchema: {
    query: getOrdersByUserSchema
  },
  postOrderSchema: {
    body: postOrderBodySchema
  }
};
