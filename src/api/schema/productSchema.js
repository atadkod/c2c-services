const postProductBodySchema = {
  type: "object",
  additionalProperties: false,
  properties: {
    name: { type: "string", minLength: 1 },
    categoryId: { type: "string", minLength: 1 },
    description: { type: "string" },
    image: { type: "string" },
    price: { type: "integer", minLength: 1 },
    brandName: { type: "string" },
    availability: { type: "boolean" },
    startTime: { type: "string" },
    endTime: { type: "string" }
  }
};

const putProductBodySchema = {
  type: "object",
  additionalProperties: false,
  properties: {
    uuid: { type: "string", minLength: 1 },
    name: { type: "string", minLength: 1 },
    categoryId: { type: "string", minLength: 1 },
    description: { type: "string" },
    image: { type: "string" },
    price: { type: "integer", minLength: 1 },
    brandName: { type: "string" },
    availability: { type: "boolean" },
    startTime: { type: "string" },
    endTime: { type: "string" }
  }
};

module.exports = {
  postProductBody: {
    body: postProductBodySchema
  },
  putProductBody: {
    body: putProductBodySchema
  }
};
