const userRegistrationSchema = {
  type: "object",
  additionalProperties: false,
  properties: {
    firstName: { type: "string", minLength: 1 },
    lastName: { type: "string", minLength: 1 },
    email: { type: "string", minLength: 1 },
    password: { type: "string", minLength: 1 },
    address: { type: "string", minLength: 1 },
    phoneNo: { type: "string", minLength: 1 },
    watchlist: { type: "object" }
  }
};

const userLoginSchema = {
  type: "object",
  additionalProperties: false,
  properties: {
    email: { type: "string", minLength: 1 },
    password: { type: "string", minLength: 1 }
  }
};

module.exports = {
  postUserRegistrationBody: {
    body: userRegistrationSchema
  },
  postUserLoginBody: {
    body: userLoginSchema
  }
};
