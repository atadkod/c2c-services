const postWatchlistBodySchema = {
  type: "object",
  additionalProperties: false,
  properties: {
    productId: { type: "string", minLength: 1 },
    buyerId: { type: "string", minLength: 1 },
    threshold: { type: "integer" }
  }
};

const putWatchlistBodySchema = {
  type: "object",
  additionalProperties: false,
  properties: {
    id: { type: "string", minLength: 1 },
    productId: { type: "string", minLength: 1 },
    buyerId: { type: "string", minLength: 1 },
    threshold: { type: "integer" }
  }
};

module.exports = {
  postWatchlist: {
    body: postWatchlistBodySchema
  },
  putWatchlist: {
    body: putWatchlistBodySchema
  }
};
