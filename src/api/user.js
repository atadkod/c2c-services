const {
  postUserRegistrationBody,
  postUserLoginBody
} = require("./schema/userSchema");
const userRepository = require("../plugins/repository/v1/userRepository");
var bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { OK } = require("http-status-codes");
const userApi = async fastify => {
  fastify.post(
    "/register",
    { schema: postUserRegistrationBody },
    async (req, reply) => {
      fastify.log.info({
        message:
          "Request to register user with name: " +
          req.body.firstName +
          req.body.lastName
      });
      const { email, password } = req.body;
      // if (!(email && password && first_name && last_name)) {
      //     reply.code(OK).send("All input is required");
      // }
      const oldUser = await userRepository.getUserByEmail(fastify, email);

      if (oldUser) {
        return reply.code(OK).send("User Already Exist. Please Login");
      }

      const encryptedPassword = await bcrypt.hash(password, 10);
      req.body.hashed_password = encryptedPassword;
      const user = await userRepository.createUser(fastify, req.body);
      delete user.hashPassword;
      const token = jwt.sign(
        { userId: user.uuid, email },
        process.env.TOKEN_KEY,
        {
          expiresIn: "2h"
        }
      );
      user.token = token;
      console.log(user);
      reply.code(OK).send(user);
    }
  );

  fastify.post("/login", { schema: postUserLoginBody }, async (req, reply) => {
    fastify.log.info({
      message: "Request to login user with email: " + req.body.email
    });
    const { email, password } = req.body;

    if (!(email && password)) {
      reply.code(400).send("All input is required");
    }

    const user = await userRepository.getUserByEmail(fastify, email);
    if (user && (await bcrypt.compare(password, user.hashPassword))) {
      const token = jwt.sign(
        { userId: user.uuid, email },
        process.env.TOKEN_KEY,
        {
          expiresIn: "2h"
        }
      );
      user.token = token;
      delete user.hashPassword;
      reply.code(OK).send(user);
    }
  });
};

module.exports = userApi;
module.exports.autoPrefix = "/user";
