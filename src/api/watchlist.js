const { OK } = require("http-status-codes");
const { postWatchlist, putWatchlist } = require("./schema/watchlistSchema");
const watchlistServie = require("../plugins/services/watchlistService");
const watchlistRepository = require("../plugins/repository/v1/watchlistRepository");
const authenticateFactory = require("../utilities/authenticate");

const watchlistAPI = async fastify => {
  fastify.post(
    "/",
    { schema: postWatchlist, preHandler: authenticateFactory() },
    async (req, reply) => {
      fastify.log.info({
        message:
          "Request to insert to watchlist for : " +
          JSON.stringify({ ...req.body, buyerId: req.user.userId })
      });

      const res = await watchlistRepository.insert(
        fastify,
        req.user.userId,
        req.body
      );
      reply.code(OK).send(res);
    }
  );

  fastify.put(
    "/setThreshold",
    { schema: putWatchlist, preHandler: authenticateFactory() },
    async (req, reply) => {
      fastify.log.info({
        message:
          "Request to fetch all products for sellerId: " + req.user.userId
      });

      const res1 = await watchlistRepository.setThreshold(fastify, req.body);
      reply.code(OK).send(res1);
    }
  );

  fastify.get("/byBuyerId", async (req, reply) => {
    fastify.log.info({
      message: "Request to fetch all watchlistItems by buyerId:" + req.query.id
    });

    const res = await watchlistRepository.getAllByBuyerId(
      fastify,
      req.query.id
    );
    reply.code(OK).send(res);
  });

  fastify.delete("/", async (req, reply) => {
    fastify.log.info({
      message: "Request to delete watchllist item by id: " + req.query.id
    });

    const res = await watchlistRepository.deleteById(fastify, req.query.id);
    reply.code(OK).send(res);
  });

  fastify.put("/byBuyerId", async (req, reply) => {
    fastify.log.info({
      message: "Request to fetch all watchlistItems by buyerId:" + req.query.id
    });

    const res = await watchlistServie.updateByBuyerId(fastify, req.query.id);
    reply.code(OK).send(res);
  });
};

module.exports = watchlistAPI;
module.exports.autoPrefix = "/watchlist";
