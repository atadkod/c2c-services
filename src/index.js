"use strict";
require("dotenv").config();
const autoload = require("fastify-autoload");
const path = require("path");
const postgres = require("./support/postgres");
// const Ajv = require("ajv");
const PORT = process.env.PORT || 8082;
const app = require("fastify")({ logger: true });

// const ajv = new Ajv({
//   removeAdditional: false,
//   useDefaults: true,
//   coerceTypes: false,
//   allErrors: true,
//   jsonPointers: true
// });

// require("ajv-keywords")(ajv, ["uniqueItemProperties"]);
// require("ajv-errors")(ajv);

// const init = fastify => {
//   //UI connection uri and port
//   // fastify.register(require("fastify-cors"), {
//   //   origin: "http://localhost:3000"
//   // });
//   const authenticateFactory = require("./utilities/authenticate");

// fastify.register(async fastify => {
//   const authenticate = authenticateFactory(fastify);
//   fastify.addHook("preValidation", authenticate);

//   fastify.register(autoload, {
//     dir: path.join(__dirname, "api"),
//     options: { prefix: "/c2c" },
//     ignorePattern: /^(__tests__|schema)/
//   });
// });

//   fastify.setValidatorCompiler(({ schema }) => ajv.compile(schema));

//   const responseTimeout = Math.trunc(process.env.TIMEOUT_SERVER) || 120000;
//   fastify.server.setTimeout(responseTimeout);

//   return fastify;
// };

// const create = () => {
//   const app = init(fastify);
//   app.register(postgres);
//   return app;
// };

// const app = create();

// Authontication header
// const authenticateFactory = require("./utilities/authenticate");
// const authenticate = authenticateFactory(app);
// app.addHook("preValidation", authenticate);

app.register(postgres);
app.register(require("fastify-cors"), {
  origin: true,
  methods: "GET, HEAD, PUT, PATCH, POST, DELETE"
});
app.register(autoload, {
  dir: path.join(__dirname, "api"),
  options: { prefix: "/" },
  ignorePattern: /^(__tests__|schema)/
});

const start = async () => {
  try {
    await app.listen(PORT, "0.0.0.0");
    app.log.info(`Server listening on ${app.server.address().port}`);
  } catch (err) {
    app.log.error(err);
    process.exit(1);
  }
};

start();
