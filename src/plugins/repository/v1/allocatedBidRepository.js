const SQL = require("@nearform/sql");

const getByProductId = async (fastify, productId) => {
  const sql = SQL`select * from allocated_bids where product_id = ${productId} and buyer_status is not false or buyer_status is null`;
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info(result.rows);
      return result.rowCount <= 0 ? null : result.rows;
    })
    .catch(err => {
      fastify.log.error({
        err,
        message:
          "Request Failed while fetching all  allocated bids for productId :" +
          productId
      });
      throw new Error(err);
    });
};

const getBySellerAndProductId = async (fastify, sellerId, productId) => {
  const sql = SQL`select * from allocated_bids where seller_id = ${sellerId} and product_id = ${productId} and buyer_status is not false or buyer_status is null`;
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info(result.rows);
      return result.rowCount <= 0 ? null : result.rows;
    })
    .catch(err => {
      fastify.log.error({
        err,
        message:
          "Request Failed while fetching all bids by sellerId: " + sellerId
      });
      throw new Error(err);
    });
};

const getBySellerId = async (fastify, sellerId) => {
  const sql = SQL`select * from allocated_bids where seller_id = ${sellerId} and buyer_status is not false or buyer_status is null`;
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info(result.rows);
      return result.rowCount <= 0 ? null : result.rows;
    })
    .catch(err => {
      fastify.log.error({
        err,
        message:
          "Request Failed while fetching all bids by sellerId: " + sellerId
      });
      throw new Error(err);
    });
};

const getByBuyerId = async (fastify, buyerId) => {
  const sql = SQL`select * from allocated_bids where buyer_id = ${buyerId} and buyer_status is null`;
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info(result.rows);
      return result.rowCount <= 0 ? null : result.rows;
    })
    .catch(err => {
      fastify.log.error({
        err,
        message:
          "Request Failed while fetching all allocated bids by beyreId: " +
          buyerId
      });
      throw new Error(err);
    });
};

const insert = async (fastify, data) => {
  const sql = buildInsertAllocBidQuery(data);
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info("Inserted row: " + result.rows);
      return result.rowCount <= 0 ? null : result.rows[0];
    })
    .catch(err => {
      fastify.log.error({
        err,
        message:
          "Request Failed while inserting allocated bid entry: " +
          JSON.stringify(data)
      });
      throw new Error(err);
    });
};

const updateBuyerStatus = async (fastify, bidId, action) => {
  const status = action ? 1 : 0;
  const sql = SQL`UPDATE allocated_bids  SET buyer_status = ${status} WHERE bid_id = ${bidId} `;
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info("Inserted row: " + result.rows);
      return result.rowCount <= 0 ? null : result.rows[0];
    })
    .catch(err => {
      fastify.log.error({
        err,
        message:
          "Request Failed while inserting allocated bid entry wwith bidId: " +
          bidId
      });
      throw new Error(err);
    });
};

const buildInsertAllocBidQuery = data => {
  const sql = SQL`INSERT INTO allocated_bids
                            (
                                bid_id,
                              product_id,
                              buyer_id,
                              seller_id,
                              price,
                              buyer_status
                            ) VALUES 
                            (
                            ${data.bidId},
                              ${data.productId},
                              ${data.buyerId},
                              ${data.sellerId},
                              ${data.price},
                              ${data.buyerStatus ? data.buyerStatus : null}
                            ) RETURNING *`;
  return sql;
};
module.exports = {
  insert,
  getByBuyerId,
  getByProductId,
  getBySellerAndProductId,
  getBySellerId,
  updateBuyerStatus
};
