const SQL = require("@nearform/sql");

const getByProductId = async (fastify, productId) => {
  const sql = SQL`select * from bid_entry where product_id = ${productId} and is_active is true order by bid_price :: numeric DESC`;
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info(result.rows);
      return result.rowCount <= 0 ? null : result.rows;
    })
    .catch(err => {
      fastify.log.error({
        err,
        message:
          "Request Failed while fetching all bids for productId :" + productId
      });
      throw new Error(err);
    });
};

const getHighestBidByProductId = async (fastify, productId) => {
  const sql = SQL`select * from bid_entry where product_id = ${productId} and is_active is true order by bid_price :: numeric DESC limit 1`;
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info(result.rows);
      return result.rowCount <= 0 ? null : result.rows[0];
    })
    .catch(err => {
      fastify.log.error({
        err,
        message:
          "Request Failed while fetching all bids for productId :" + productId
      });
      throw new Error(err);
    });
};

const getBySellerAndProductId = async (fastify, sellerId, productId) => {
  const sql = SQL`select * from bid_entry where seller_id = ${sellerId} and product_id = ${productId} and is_active is true order by bid_price :: numeric DESC `;
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info(result.rows);
      return result.rowCount <= 0 ? null : result.rows;
    })
    .catch(err => {
      fastify.log.error({
        err,
        message:
          "Request Failed while fetching all bids by sellerId: " + sellerId
      });
      throw new Error(err);
    });
};

const getByBuyerId = async (fastify, buyerId) => {
  const sql = SQL`select * from bid_entry where buyer_id = ${buyerId} and is_active is true`;
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info(result.rows);
      return result.rowCount <= 0 ? null : result.rows;
    })
    .catch(err => {
      fastify.log.error({
        err,
        message: "Request Failed while fetching all bids by beyreId: " + buyerId
      });
      throw new Error(err);
    });
};

const insert = async (fastify, data) => {
  const sql = buildInsertBidQuery(data);
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info("Inserted row: " + result.rows);
      return result.rowCount <= 0 ? null : result.rows;
    })
    .catch(err => {
      fastify.log.error({
        err,
        message:
          "Request Failed while inserting bid entry: " + JSON.stringify(data)
      });
      throw new Error(err);
    });
};

const invalidateBidsByProductId = async (fastify, bidId) => {
  const sql = SQL`UPDATE bid_entry SET is_active = False WHERE bid_id = ${bidId}`;
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info("Invalidating all bids for productId : " + bidId);
      return result.rowCount <= 0 ? null : result.rowCount;
    })
    .catch(err => {
      fastify.log.error({
        err,
        message:
          "Request Failed while Invalidating all bids for productId : " + bidId
      });
      throw new Error(err);
    });
};

const invalidatebidEntryByBuyerAndProductId = async (
  fastify,
  productId,
  buyerId
) => {
  const sql = SQL`UPDATE bid_entry SET is_active = 0 WHERE buyer_id =${buyerId} and  product_id = ${productId}`;
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info("Invalidating all bids for productId : " + productId);
      return result.rowCount <= 0 ? null : result.rowCount;
    })
    .catch(err => {
      fastify.log.error({
        err,
        message:
          "Request Failed while Invalidating all bids for productId : " +
          productId
      });
      throw new Error(err);
    });
};

const buildInsertBidQuery = data => {
  const sql = SQL`INSERT INTO bid_entry
                            (
                              product_id,
                              buyer_id,
                              seller_id,
                              base_price,
                              bid_price,
                              message,
                              is_active
                            ) VALUES 
                            (
                              ${data.productId},
                              ${data.buyerId},
                              ${data.sellerId},
                              ${data.basePrice},
                              ${data.bidPrice},
                              ${data.message ? data.message : null},
                              ${data.isActive != null ? data.isActive : true}
                            ) RETURNING *`;
  return sql;
};
module.exports = {
  insert,
  getByBuyerId,
  getByProductId,
  getBySellerAndProductId,
  invalidateBidsByProductId,
  invalidatebidEntryByBuyerAndProductId,
  getHighestBidByProductId
};
