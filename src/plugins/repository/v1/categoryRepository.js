const SQL = require("@nearform/sql");

const getParentCategories = async fastify => {
  const sql = SQL`select * from category where parent_id is null`;
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info(result.rows);
      return result.rowCount <= 0 ? null : result.rows;
    })
    .catch(err => {
      fastify.log.error({
        err,
        message: "Request Failed while fetching parent categories types"
      });
      throw new Error(err);
    });
};

const getCategoryByName = async (fastify, name) => {
  const sql = SQL`select * from category where name = ${name}`;
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info(result.rows);
      return result.rowCount <= 0 ? null : result.rows;
    })
    .catch(err => {
      fastify.log.error({
        err,
        message: "Request Failed while fetching Category by name"
      });
      throw new Error(err);
    });
};

const getCategoryById = async (fastify, id) => {
  const sql = SQL`select * from category where id = ${id}`;
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info(result.rows);
      return result.rowCount <= 0 ? null : result.rows[0];
    })
    .catch(err => {
      fastify.log.error({
        err,
        message: "Request Failed while fetching Category by id"
      });
      throw new Error(err);
    });
};
const getCategoryByParentId = async (fastify, id) => {
  const sql = SQL`select * from category where parent_id = ${id}`;
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info(result.rows);
      return result.rowCount <= 0 ? null : result.rows;
    })
    .catch(err => {
      fastify.log.error({
        err,
        message: "Request Failed while fetching Sub Category by parent id"
      });
      throw new Error(err);
    });
};
const postCategory = async (fastify, category) => {
  const sql = buildInsetCategoryQuery(category);
  console.log(category);
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info("Inserted row: " + result.rows);
      return result.rowCount <= 0 ? null : result.rows;
    })
    .catch(err => {
      fastify.log.error({
        err,
        message: "Request Failed while inserting category"
      });
      throw new Error(err);
    });
};

const buildInsetCategoryQuery = data => {
  const sql = SQL`INSERT INTO category
                        (
                          name,
                          parent_id,
                          is_active
                        ) VALUES 
                        (
                          ${data.name},
                          ${data.parentId ? data.parentId : null},
                          ${data.isActive ? data.isActive : true}
                        ) RETURNING *`;
  return sql;
};

module.exports = {
  getParentCategories,
  getCategoryByName,
  getCategoryById,
  postCategory,
  getCategoryByParentId
};
