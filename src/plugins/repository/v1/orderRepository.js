const SQL = require("@nearform/sql");

const postOrder = async (fastify, order, id) => {
  const sql = buildInsetOrderQuery(order, id);

  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info("Inserted row: " + result.rows);
      return result.rowCount <= 0 ? null : result.rows;
    })
    .catch(err => {
      fastify.log.error({
        err,
        message: "Request Failed while inserting order"
      });
      throw new Error(err);
    });
};

const getSoldOrders = async (fastify, id) => {
  const sql = SQL`Select * from orders where seller_id=${id}`;
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info("Retrived row: " + result.rows);
      return result.rowCount <= 0 ? null : result.rows;
    })
    .catch(err => {
      fastify.log.error({
        err,
        message: "Request Failed while retreiving sold order"
      });
      throw new Error(err);
    });
};

const getOrders = async (fastify, id) => {
  const sql = SQL`Select * from orders where seller_id=${id} or buyer_id=${id}`;
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info("Retrived row: " + result.rows);
      return result.rowCount <= 0 ? null : result.rows;
    })
    .catch(err => {
      fastify.log.error({
        err,
        message: "Request Failed while retreiving  all order"
      });
      throw new Error(err);
    });
};

const getBoughtOrders = async (fastify, id) => {
  const sql = SQL`Select * from orders where seller_id=${id}`;
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info("Retrived row: " + result.rows);
      return result.rowCount <= 0 ? null : result.rows;
    })
    .catch(err => {
      fastify.log.error({
        err,
        message: "Request Failed while retreiving bought order"
      });
      throw new Error(err);
    });
};

const buildInsetOrderQuery = (data, id) => {
  const sql = SQL`INSERT INTO orders
                          (
                            product_id,
                            buyer_id,
                            seller_id,
                            price
                          ) VALUES 
                          (
                            ${data.productId},
                            ${data.buyerId},
                            ${id},
                            ${data.price}
                          ) RETURNING *`;
  return sql;
};

module.exports = {
  postOrder,
  getSoldOrders,
  getBoughtOrders,
  getOrders
};
