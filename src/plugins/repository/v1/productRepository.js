const SQL = require("@nearform/sql");

const getAllBuyerProduct = async (fastify, sellerId) => {
  console.log("###" + JSON.stringify(sellerId));
  const sql = sellerId
    ? SQL`select * from products where availability is true and seller_id <> ${sellerId}`
    : SQL`select * from products where availability is true`;

  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info(result.rows);
      return result.rowCount <= 0 ? null : result.rows;
    })
    .catch(err => {
      fastify.log.error({
        err,
        message: "Request Failed while fetching all product"
      });
      throw new Error(err);
    });
};

const getAllSellerProduct = async (fastify, sellerId) => {
  console.log("###" + JSON.stringify(sellerId));
  const sql = sellerId
    ? SQL`select * from products where availability is true and seller_id = ${sellerId}`
    : SQL`select * from products where availability is true`;

  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info(result.rows);
      return result.rowCount <= 0 ? null : result.rows;
    })
    .catch(err => {
      fastify.log.error({
        err,
        message: "Request Failed while fetching all product"
      });
      throw new Error(err);
    });
};

const getByCategoryId = async (fastify, categoryId) => {
  const sql = SQL`select * from products where category_id = ${categoryId}`;
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info(result.rows);
      return result.rowCount <= 0 ? null : result.rows;
    })
    .catch(err => {
      fastify.log.error({
        err,
        message:
          "Request Failed while fetching products by Category id :" + categoryId
      });
      throw new Error(err);
    });
};

const getByProductId = async (fastify, productId) => {
  const sql = SQL`select * from products where uuid = ${productId} and availability=True`;
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info(result.rows);
      return result.rowCount <= 0 ? null : result.rows[0];
    })
    .catch(err => {
      fastify.log.error({
        err,
        message:
          "Request Failed while fetching products by Category id :" + productId
      });
      throw new Error(err);
    });
};

const getByProductIdWithoutAvailability = async (fastify, productId) => {
  const sql = SQL`select * from products where uuid = ${productId}`;
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info(result.rows);
      return result.rowCount <= 0 ? null : result.rows[0];
    })
    .catch(err => {
      fastify.log.error({
        err,
        message:
          "Request Failed while fetching products by Category id :" + productId
      });
      throw new Error(err);
    });
};

const insert = async (fastify, product) => {
  const sql = buildInsetProductQuery(product);
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info("Inserted row: " + result.rows);
      return result.rowCount <= 0 ? null : result.rows;
    })
    .catch(err => {
      fastify.log.error({
        err,
        message: "Request Failed while inserting product: " + product
      });
      throw new Error(err);
    });
};

const update = async (fastify, product) => {
  const sql = buildUpdateProductQuery(product);
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info("Updated row: " + result.rows);
      return result.rowCount <= 0 ? null : result.rows;
    })
    .catch(err => {
      fastify.log.error({
        err,
        message: "Request Failed while updating product: " + product
      });
      throw new Error(err);
    });
};

const notAvailable = async (fastify, productId) => {

  const sql = `UPDATE products SET availability = False WHERE uuid = '${productId}` + "'";
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info("Updated row: " + result.rows);
      return result.rowCount <= 0 ? null : result.rows;
    })
    .catch(err => {
      fastify.log.error({
        err,
        message:
          "Request Failed while updating product vailability to false for : " +
          productId
      });
      throw new Error(err);
    });
};

const buildInsetProductQuery = data => {
  const sql = SQL`INSERT INTO products
                        (
                          name,
                          category_id,
                          description,
                          image,
                          price,
                          brand_name,
                          availability,
                          seller_id,
                          start_time,
                          end_time
                        ) VALUES 
                        (
                          ${data.name},
                          ${data.categoryId},
                          ${data.description ? data.description : null},
                          ${data.image ? data.image : null},
                          ${data.price ? data.price : null},
                          ${data.brandName ? data.brandName : null},
                          ${data.availability ? data.availability : true},
                          ${data.sellerId},
                          ${data.startTime ? data.startTime : ""},
                          ${data.endTime ? data.endTime : ""}
                        ) RETURNING *`;
  return sql;
};

const buildUpdateProductQuery = data => {
  const sql = SQL` UPDATE products
                        SET
                        name=${data.name},
                        category_id=${data.categoryId},
                        description=${data.description ? data.description : null
    },
                        image= ${data.image ? data.image : null},
                        price=${data.price ? data.price : null},
                        brand_name =  ${data.brandName ? data.brandName : null},
                        availability =${data.availability ? data.availability : true
    }
                        start_time=${data.startTime ? data.startTime : null},
                        end_time =  ${data.endTime ? data.endTime : null}
                        WHERE uuid = ${data.uuid}
                        AND
                        seller_id = ${data.sellerId}
                        RETURNING *`;
  return sql;
};

module.exports = {
  insert,
  update,
  getAllBuyerProduct,
  getAllSellerProduct,
  getByCategoryId,
  notAvailable,
  getByProductId,
  getByProductIdWithoutAvailability
};
