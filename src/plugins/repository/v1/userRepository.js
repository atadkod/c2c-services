const SQL = require("@nearform/sql");

const getUserByEmail = async (fastify, email) => {
  const sql = SQL`select * from users where email=${email}`;
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info(result.rows);
      return result.rowCount <= 0 ? null : result.rows[0];
    })
    .catch(err => {
      fastify.log.error({
        err,
        message: "Request Failed while fetching all product"
      });
      throw new Error(err);
    });
};

const createUser = async (fastify, user) => {
  const sql = buildInsertUserQuery(user);
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info("Inserted row: " + result.rows);
      return result.rowCount <= 0 ? null : result.rows[0];
    })
    .catch(err => {
      fastify.log.error({
        err,
        message: "Request Failed while inserting user: " + user.firstName
      });
      throw new Error(err);
    });
};

const completedRatingUpdate = async (fastify, userId, action) => {
  const count = action ? 1 : -1;
  const sql = SQL`UPDATE users SET  completed_orders_count = completed_orders_count + ${count} WHERE uuid =${userId}`;
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info("Inserted row: " + result.rows);
      return result.rowCount <= 0 ? null : result.rows[0];
    })
    .catch(err => {
      fastify.log.error({
        err,
        message: "Request Failed while updating ratings of  user: " + userId
      });
      throw new Error(err);
    });
};

const totalRatingUpdate = async (fastify, userId, action) => {
  const count = action ? 1 : -1;
  const sql = SQL`UPDATE users SET  total_rating_count = total_rating_count + ${count} WHERE uuid =${userId}`;
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info("Inserted row: " + result.rows);
      return result.rowCount <= 0 ? null : result.rows[0];
    })
    .catch(err => {
      fastify.log.error({
        err,
        message: "Request Failed while updating ratings of  user: " + userId
      });
      throw new Error(err);
    });
};

const buildInsertUserQuery = data => {
  const sql = SQL`INSERT INTO users
                            (
                              firstname,
                              lastname,
                              email,
                              hash_password,
                              address,
                              phoneno
                            ) VALUES 
                            (
                              ${data.firstName},
                              ${data.lastName},
                              ${data.email},
                              ${data.hashed_password},
                              ${data.address},
                              ${data.phoneNo}
                            ) RETURNING *`;
  return sql;
};

module.exports = {
  getUserByEmail,
  createUser,
  completedRatingUpdate,
  totalRatingUpdate
};
