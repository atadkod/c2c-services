const SQL = require("@nearform/sql");

const insert = async (fastify, buyerId, data) => {
  const sql = buildInsertWatchlistQuery(data, buyerId);
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info("Inserted row: " + result.rows);
      return result.rowCount <= 0 ? null : result.rows[0];
    })
    .catch(err => {
      fastify.log.error({
        err,
        message:
          "Request Failed while inserting watchlist of buyerId: " + buyerId
      });
      throw new Error(err);
    });
};

const setThreshold = async (fastify, data) => {
  console.log(data);
  const sql = buildSetThresholdWatchlistQuery(data);
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info("setting threshold row: " + result.rows);
      return result.rowCount <= 0 ? null : result.rows[0];
    })
    .catch(err => {
      fastify.log.error({
        err,
        message: "Request Failed while etting threshold watchlist: " + data
      });
      throw new Error(err);
    });
};

const deleteById = async (fastify, id) => {
  const sql = SQL`DELETE FROM watchlist where id =${id}`;
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info("Inserted row: " + result.rows);
      return result.rowCount <= 0 ? null : result.rows[0];
    })
    .catch(err => {
      fastify.log.error({
        err,
        message: "Request Failed while deleteing watchlist of id: " + id
      });
      throw new Error(err);
    });
};

const getAllByBuyerId = async (fastify, buyerId) => {
  const sql = SQL`SELECT * FROM watchlist where buyer_id =${buyerId}`;
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info("Inserted row: " + result.rows);
      return result.rowCount <= 0 ? null : result.rows;
    })
    .catch(err => {
      fastify.log.error({
        err,
        message:
          "Request Failed while get all watchlist items of buyerId: " + buyerId
      });
      throw new Error(err);
    });
};

const updateStatusAsPerThresholdByProductId = async (
  fastify,
  productId,
  price
) => {
  const sql = SQL`UPDATE watchlist SET threshold_status = True where product_id =${productId} and threshold < ${price}`;
  return await fastify.pg
    .query(sql)
    .then(result => {
      fastify.log.info(
        "updating threshold status of watchlist items for: " + result.rows
      );
      return result.rowCount <= 0 ? null : result.rows;
    })
    .catch(err => {
      fastify.log.error({
        err,
        message:
          "Request Failed while updating threshold status  of watchlist items for : " +
          productId
      });
      throw new Error(err);
    });
};

const buildInsertWatchlistQuery = (data, buyerId) => {
  console.log("@@@@@@@@ : ", data, buyerId);
  return SQL`INSERT INTO watchlist (
            product_id,
            buyer_id,
            threshold,
            threshold_status   
      ) VALUES (
        ${data.productId},
        ${buyerId},
        ${data.threshold},
        ${data.thresholdSatus ? data.thresholdSatus : false}
      ) RETURnING *`;
};

const buildSetThresholdWatchlistQuery = data => {
  return SQL`UPDATE watchlist SET
     threshold = ${data.threshold}
    WHERE id=${data.id}
     RETURnING *`;
};

module.exports = {
  insert,
  setThreshold,
  deleteById,
  getAllByBuyerId,
  updateStatusAsPerThresholdByProductId
};
