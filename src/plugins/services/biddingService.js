const productRepository = require("../repository/v1/productRepository");
const bidRepository = require("../repository/v1/biddingResitory");
const allocatedBidRepository = require("../repository/v1/allocatedBidRepository");
const userRepository = require("../repository/v1/userRepository");
const ordersRepository = require("../repository/v1/orderRepository");

const insert = async (fastify, data) => {
  fastify.log.info({
    message: "service for adding bid entry for bid:" + JSON.stringify(data)
  });

  if (data.bidPrice < data.basePrice) {
    fastify.log.info({
      message:
        "nid price: " +
        data.bidPrice +
        " is less than base price: " +
        data.basePrice
    });
    return null;
  }

  try {
    return await bidRepository.insert(fastify, data);
  } catch (err) {
    fastify.log.error({
      message: "Error adding bid for " + JSON.stringify(data),
      error: err
    });
  }
};

const confirmedBySeller = async (fastify, bidData) => {
  fastify.log.info({
    message: "Confirmation by seller service for bid :" + bidData
  });
  //update products table availability to false
  //insert to  allocated_bid
  // increment count for total rating in users table for that buyer
  // change the is_active for all the bids of that product to false -- not included
  await productRepository.notAvailable(fastify, bidData.productId);

  const res = await allocatedBidRepository.insert(fastify, bidData);

  await userRepository.totalRatingUpdate(fastify, bidData.buyerId, true);

  await bidRepository.invalidateBidsByProductId(fastify, bidData.bidId);

  return res;
};

const ActionByBuyer = async (fastify, bidData, confirmed) => {
  fastify.log.info({
    message: "Action  by buyer service for bid :" + bidData
  });
  // update is_active in bid_entry to false for this buyerId
  //true = in_bid, false =allocated_bid
  await bidRepository.invalidateBidsByProductId(
    fastify,
    bidData.productId,
    bidData.buyerId
  );
  if (confirmed) {
    //update buyer_status to true in allocated table
    //add entry to orders table
    //increment completed_orders_count in users table for this buyer id
    await allocatedBidRepository.updateBuyerStatus(fastify, bidData.bidId, true);

    await ordersRepository.postOrder(fastify, bidData, bidData.sellerId);

    await userRepository.completedRatingUpdate(fastify, bidData.buyerId, true);
  } else {
    //update buyer_status to false in allocated table
    //decrement completed_orders_count in users table

    await allocatedBidRepository.updateBuyerStatus(fastify, bidData.bidId, false);

    await userRepository.completedRatingUpdate(fastify, bidData.buyerId, false);
  }
  //buyer_status
  //null: pending,  true: confirm , false :denied
};

module.exports = {
  insert,
  confirmedBySeller,
  ActionByBuyer
};
