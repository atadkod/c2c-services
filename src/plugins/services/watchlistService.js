const watchlistRepo = require("../repository/v1/watchlistRepository");
const bidRepo = require("../repository/v1/biddingResitory");
const updateByBuyerId = async (fastify, buyerId) => {
  fastify.log.info({
    message: "service for fetch all watchlist items by buyerId:"
  });
  // update the threshold_status of all the products for this buyer_id
  // fetch from watchlist table by buyerid
  try {
    const res = await watchlistRepo.getAllByBuyerId(fastify, buyerId);
    console.log(res);
    res.forEach(async element => {
      const bidEntry = await bidRepo.getHighestBidByProductId(
        fastify,
        element.productId
      );
      return await watchlistRepo.updateStatusAsPerThresholdByProductId(
        fastify,
        bidEntry.productId,
        bidEntry.bidPrice
      );
    });
  } catch (err) {
    fastify.log.info({
      message:
        "falied service for fetch all watchlist items by buyerId:" + buyerId,
      error: err
    });
  }
};

module.exports = {
  updateByBuyerId
};
