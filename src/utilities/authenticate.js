const { UNAUTHORIZED } = require("http-status-codes");
const jwt = require("jsonwebtoken");
const authenticateFactory = () => {
  return async request => {
    const authInfo = request.headers["x-access-token"];
    if (!authInfo) {
      const error = { status: UNAUTHORIZED, detail: "ACCESS DENIED" };
      throw new Error(error);
    } else {
      try {
        const decoded = jwt.verify(authInfo, process.env.TOKEN_KEY);
        request.user = decoded;
      } catch (err) {
        const error = { status: UNAUTHORIZED, detail: "ACCESS DENIED" };
        throw new Error(error);
      }
    }
    return;
  };
};

module.exports = authenticateFactory;
