const envSchema = require("env-schema");

const schema = {
  type: "object",
  properties: {
    CACHE_CONTROL: {
      type: "string",
      default: "public"
    },
    MAX_AGE: {
      type: "string",
      default: "300"
    }
  }
};

const config = () =>
  envSchema({
    schema: schema,
    dotenv: true
  });
module.exports = config;
