const constants = {
  PSQL_RESTRICT_VIOLATION_ERROR_CODE: "23001",
  CARDINALITY_VIOLATION: "21000",
  PSQL_DUPLICATE_KEY_VIOLATION_ERROR_CODE: "23505",
  PSQL_FOREIGN_KEY_VIOLATION_ERROR_CODE: "23503",
  PSQL_CARDINALITY_VIOLATION: "21000",
  ENTITY: "Entity",
  AUTHOR: "Author",
  REVIEWS: "Reviews",
  AUTHORS_REVIEW: "Author's review",
  RATINGS: "Ratings",
  RATINGS_META_DATA: "Ratings Meta Data",
  AVERAGE_RATINGS: "Average Ratings",
  POST: "POST",
  GET: "GET",
  PATCH: "PATCH",
  DELETE: "DELETE",
  version: {
    VERSION_ONE: "v1",
    VERSION_TWO: "v2"
  },
  SUCCESS: "success",
  FAILED: "failed",
  IN_PROGRESS: "in_progress",
  NOT_AVAILABLE: "NA",

  REVIEW_STATUS: {
    excellent: "EXCELLENT",
    good: "GOOD",
    average: "AVERAGE",
    notGood: "NOT_GOOD",
    bad: "BAD",
    accepted: "ACCEPTED",
    rejected: "REJECTED",
    pending: "PENDING",
    invalid: "INVALID"
  },

  //GENERIC DB ERRORS
  OLD_DATA_ERROR: {
    name: "OLD_DATA_ERROR",
    title: "Trying to update with old data, try with newer date",
    titleES: "Actualizacion es antiguo/invalido, intente con nueva fecha"
  },
  SCHEMA_ERROR: {
    name: "SCHEMA_ERROR",
    title: "The request data violates the schema constraints",
    titleES: "Data es invalido, no cumple con esquema requerido"
  },
  FK_VIOLATION: {
    name: "FK_VIOLATION",
    title: `Foreign Key constraint violation`,
    titleES: `Infraccion de restriccion de clave externa`
  },
  DUPLICATE_KEY_VIOLATION: {
    name: "DUPLICATE_KEY_VIOLATION",
    title: `Duplicate Key constraint violation`,
    titleES: `Infraccion de restriccion de clave duplicada`
  },
  CXT_TIMEOUT: {
    name: "Connection timeout",
    title: `Connection terminated due to timeout.`,
    titleES: `Conexion terminada debido al tiempo de espera.`
  },
  GENERIC_SERVER_ERROR: {
    name: "GENERIC_SERVER_ERROR",
    title: `The Server encountered an internal error or misconfiguration and was unable to complete the request.`,
    titleES: `El servidor encontro un error interno o una configuracion incorrecta y no pudo completar la solicitud.`
  },
  DATA_NOT_FOUND: {
    name: "DATA_NOT_FOUND",
    title: `The requested data is not available at the moment. Please try later.`,
    titleES: `Los datos solicitados no están disponibles en este momento. Por favor intente mas tarde.`
  }
};

module.exports = constants;
