const envSchema = require("env-schema");

const schema = {
  type: "object",
  properties: {
    ENV_EXAMPLE: {
      type: "number",
      default: 10
    }
  }
};

const config = () =>
  envSchema({
    schema: schema,
    dotenv: true
  });
module.exports = config;
