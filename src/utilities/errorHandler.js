const {
  getStatusText,
  CONFLICT,
  BAD_REQUEST,
  PRECONDITION_FAILED,
  INTERNAL_SERVER_ERROR,
  SERVICE_UNAVAILABLE,
  NOT_FOUND
} = require("http-status-codes");
const constants = require("../utilities/constants");

const errorHandlerFactory = () => {
  const { ApiError } = new Error();

  class C2CApiError extends ApiError {
    constructor({ id, status, code, title, detail, meta }) {
      super({ id, status, code, title: detail, detail, meta });
      this.title = title;
    }
  }

  const getError = (info, status, detail, code, meta) => {
    return new C2CApiError({
      code: code || info.name,
      status,
      title: {
        en: info.title,
        es: info.titleES
      },
      detail,
      meta
    });
  };

  const exports = {};

  exports.dbError = error => {
    let status;
    let detail;
    let code;
    let info;
    if (error.code === constants.PSQL_RESTRICT_VIOLATION_ERROR_CODE) {
      status = CONFLICT;
      code = getStatusText(CONFLICT);
      info = constants.OLD_DATA_ERROR;
      detail = `${error.detail}. Please try again with the updated data`;
    } else if (error.code === constants.PSQL_CARDINALITY_VIOLATION) {
      status = BAD_REQUEST;
      code = getStatusText(BAD_REQUEST);
      info = constants.SCHEMA_ERROR;
      detail =
        error.hint ||
        "Same variant or site is repeating. Please try again after removing the duplicate data";
    } else if (
      error.code === constants.PSQL_DUPLICATE_KEY_VIOLATION_ERROR_CODE
    ) {
      status = BAD_REQUEST;
      code = getStatusText(BAD_REQUEST);
      info = constants.DUPLICATE_KEY_VIOLATION;
      detail = error.detail;
    } else if (error.code === constants.PSQL_FOREIGN_KEY_VIOLATION_ERROR_CODE) {
      status = PRECONDITION_FAILED;
      code = getStatusText(PRECONDITION_FAILED);
      info = constants.FK_VIOLATION;
      detail = error.detail;
    } else if (
      error.message === "Connection terminated due to connection timeout" ||
      error.message === "timeout exceeded when trying to connect"
    ) {
      status = SERVICE_UNAVAILABLE;
      code = getStatusText(SERVICE_UNAVAILABLE);
      info = constants.CXT_TIMEOUT;
      detail = "DB connection timeout";
    } else {
      status = INTERNAL_SERVER_ERROR;
      code = getStatusText(INTERNAL_SERVER_ERROR);
      info = constants.GENERIC_SERVER_ERROR;
      detail = error.detail || error.message;
    }
    return getError(info, status, detail, code);
  };

  exports.dataNotFound = (dataType, id) => {
    const detail = id
      ? `Requested ${dataType} details for ${id} is not present at this moment.`
      : `Requested ${dataType} details are not present in the DB at this moment.`;
    return getError(constants.DATA_NOT_FOUND, NOT_FOUND, detail);
  };

  return exports;
};

module.exports = errorHandlerFactory;
