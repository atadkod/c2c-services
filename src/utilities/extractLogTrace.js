const c2cConfig = require("./c2cConfig");

module.exports = (request, reply, next) => {
  request.logTrace = {
    "x-request-id": request.headers["x-request-id"],
    "x-b3-traceid": request.headers["x-b3-traceid"]
  };

  if (request.raw && request.raw.method === "GET") {
    reply.header(
      "Cache-Control",
      c2cConfig().CACHE_CONTROL + ", max-age=" + c2cConfig().MAX_AGE
    );
  }

  next();
};
